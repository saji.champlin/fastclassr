# creates a TF.data pipeline. also contains functions for preprocessing data

import numpy as np
import tensorflow as tf
import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq
import gzip
import sys
from pathlib import Path
from typing import List
from tqdm import tqdm
from sklearn import preprocessing
label_map = {
    90: "SNIa",
    67: "SNIa-91bg",
    52: "SNIax",
    42: "SNII",
    62: "SNIbc",
    95: "SLSN-I",
    15: "TDE",
    64: "KN",
    88: "AGN",
    # 92: "RRL",
    # 65: "M-dwarf",
    # 16: "EB",
    # 53: "Mira",
    6: "μLens-Single",
    991: "μLens-Binary",
    992: "ILOT",
    993: "CaRT",
    994: "PISN",
    # 995: "μLens-String"
}
wanted_classes = list(label_map.keys())
wanted_classes.sort()
# take csv files and make them into more usable formats
lb = preprocessing.LabelBinarizer()
lb.fit(wanted_classes)


def _convert_df(metadata, curve_df):

    # remove unwanted rows from files (assume metadata is pre-filtered)
    curve_df = curve_df[curve_df.index.isin(metadata.index)]
    # Phase 1: combining files
    # Create pandas dataframe that has the transient info as colums
    # rather than this dual-file method.
    # This makes storing/accessing/lookup easier.
    objs = curve_df.index.unique()
    new_meta = metadata[metadata.index.isin(objs)]  # filter meta
    curve_lists = curve_df.groupby(level=0).agg(list)  # non-vectored, slow
    new_meta['flux'] = curve_lists['flux']
    new_meta['flux_err'] = curve_lists['flux_err']
    new_meta['mjd'] = curve_lists['mjd']  # we can normalize here
    new_meta['passband'] = curve_lists['passband']
    new_meta['detected_bool'] = curve_lists['detected_bool']
    # new meta is now the standard metadata but with new columns containing arrays
    # of mjd flux passband and detected
    return new_meta


def convert_plasticc_dataset(data_path: Path, meta_file: Path, curve_files: List[Path], use_gzip = True):
    # reads the metadata and observations to output to arrow format
    if use_gzip:
        with gzip.open(meta_file, 'rt') as f:
            metadata = pd.read_csv(f)
    else:
        metadata = pd.read_csv(meta_file)
    metadata.set_index("object_id", inplace=True)
    metadata = metadata[metadata['true_target'].isin(wanted_classes)]
    print(curve_files)
    for c in tqdm(curve_files):
        print(f"reading {c}")
        if use_gzip:
            with gzip.open(c, 'rt') as cf:
                curve_df = pd.read_csv(cf)
        else:
            curve_df = pd.read_csv(c)
        # process the curve and create the output
        curve_df.set_index('object_id', inplace=True)
        out = _convert_df(metadata, curve_df)
        out.reset_index(inplace=True)
        new_filename = Path(c.stem.split('.')[0] + ".feather")
        out.to_feather(data_path / new_filename)


def get_tf_dataset(dataset: Path, batch_size=10, use_onehot=True):
    # returns a tf.data.Dataset based on the parquet file provided.
    print(f"loading dataset {dataset}")
    file = pq.ParquetFile(dataset)
    df = file.read().to_pandas()
    df['observations'] = df.apply(lambda x: np.array([x.mjd, x.passband, x.flux, x.flux_err, x.detected_bool]).T, axis=1) # FIXME: use generator
    print(f"transpose complete. Size in memory: {sys.getsizeof(df)}")

    if (use_onehot):
        def row2tensor(row):
            return (
                (
                    tf.constant((row.mjd, row.passband, row.flux, row.flux_err, row.detected_bool)), #observations
                    tf.constant((row['hostgal_photoz'], row['mwebv'])) 
                ),
                tf.constant(lb.transform([row['true_target']])[0])
            )
        output_shapes = (
            (
                tf.TensorShape([5, None]),
                tf.TensorShape([2])
            ),
            tf.TensorShape([len(wanted_classes)])
            # FIXME: replace with batch-level transformations.
        )
    else:
        def row2tensor(row):
            return (
                (
                    tf.constant((row.mjd, row.passband, row.flux, row.flux_err,
                                 row.detected_bool)),  # observations
                    tf.constant((row['hostgal_photoz'], row['mwebv']))
                ),
                tf.constant(row['true_target'])
            )
        output_shapes = (
            (
                tf.TensorShape([5, None]),
                tf.TensorShape([2])
            ),
            tf.TensorShape([])
        )

    def gen():
        for i in df.index:
            row = df.loc[i, :]
            yield row2tensor(row)
    output_types = (
        (
            tf.dtypes.float64,
            tf.dtypes.float64
        ),
        tf.dtypes.int64
    )

    ds = tf.data.Dataset.from_generator(gen, output_types, output_shapes)
    ds = ds.shuffle(500)
    ds = ds.padded_batch(batch_size, drop_remainder=True)
    return ds
