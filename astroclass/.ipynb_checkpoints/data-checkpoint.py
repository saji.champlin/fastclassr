# creates a TF.data pipeline. also contains functions for preprocessing data

import numpy as np
import tensorflow as tf
import pandas as pd

label_map = {
90: "SNIa",
67: "SNIa-91bg",
52: "SNIax",
42: "SNII",
62: "SNIbc",
95: "SLSN-I",
15: "TDE",
64: "KN",
88: "AGN",
# 92: "RRL",
# 65: "M-dwarf",
# 16: "EB",
# 53: "Mira",
6: "μLens-Single",
991: "μLens-Binary",
992: "ILOT",
993: "CaRT",
994: "PISN",
995: "μLens-String"
}