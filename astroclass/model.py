# defines the model we use

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import numpy as np
from .data import label_map
# define the model
num_classes = len(label_map.keys())
def get_model(batch_size = 10):
    # returns the model
    ts_input = layers.Input(shape=(None, 5), sparse=False, batch_size=batch_size, ragged=False, name="curves", dtype=tf.float32)
    meta_input = layers.Input(shape=(2,), batch_size=batch_size, name="metadata", dtype=tf.float32)
    lstm_1 = layers.LSTM(500, return_sequences=True, name="lstm-1")(ts_input)
    lstm_2 = layers.LSTM(500, return_sequences=True, name="lstm-2")(lstm_1)

    #repeated_meta = layers.RepeatVector(128)(meta_input)
    #x = layers.concatenate([lstm_2, layers.RepeatVector(128)(meta_input)])
    predictions = layers.Dense(num_classes, name="prediction", activation="softmax", use_bias=False)(lstm_2)
    
    last_timestep = layers.Lambda(lambda t: t[:, -1, :])(predictions)
    model = keras.Model(
        inputs=[ts_input, meta_input],
        outputs=[last_timestep]
    )
    sequence_model = keras.Model(
        inputs=[ts_input, meta_input],
        outputs=[predictions]
    )
    model.summary()
    model.compile(
        optimizer=keras.optimizers.Adam(),
        loss=[tf.keras.losses.CategoricalCrossentropy()],
        metrics=[keras.metrics.CategoricalAccuracy()]
    )
    return model


def get_simple_model(batch_size=10):
    # a really simple test model.
    ts_input = layers.Input(shape=(None, 5), sparse=False,
                            batch_size=batch_size, ragged=False,
                            name="curves", dtype=tf.float32)
    lstm = layers.LSTM(250, return_sequences=False, name="lstm")(ts_input)
    prediction = layers.Dense(num_classes, activation="softmax")(lstm)
    model = keras.Model(
        inputs=[ts_input],
        outputs=prediction
    )
    return model


def train_model(model, data, n_epochs=3):
    model.fit(data, epochs=n_epochs)
