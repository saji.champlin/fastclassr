# This takes input data and converts it to feather data stores

import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from pathlib import Path
import astroclass.data as data
import astroclass.model as model
import datetime
data_path = Path("data/")
lightcurves = list(data_path.glob("plasticc_test_lightcurves*.csv"))
md = data_path / "plasticc_test_metadata.csv"
data.convert_plasticc_dataset(data_path, md, lightcurves, use_gzip=False)