# converts the data in parquet form to tfrecord form 
# TODO: eliminate parquet step?

import tensorflow as tf
import numpy as np
import sys
sys.path.append('.')
from astroclass import data
from pathlib import Path

def make_tfrecord(parq: Path):
    # makes a tf record from a parquet dataset.
    ds = data.get_tf_dataset(parq)
    new_filename = Path(parq.stem.split('.')[0] + ".tfrecord")
    writer = tf.data.experimental.TFRecordWriter(new_filename)
    writer.write(ds)

parquets = Path("data/").glob("*.parquet")
for f in parquets:
    make_tfrecord(f)
