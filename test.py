import tensorflow as tf
from tensorflow.keras import layers, Model, Sequential
import numpy as np

x = tf.RaggedTensor.from_row_splits(np.ones((100, 20, 1)), [0, 4, 20, 100])
y = np.ones((3, 1))

model = Sequential()
model.add(layers.Input(shape=(None, 20, 1), ragged=True)) #this input with ragged=True is important
model.add(layers.TimeDistributed(layers.LSTM(32, dropout=0.4)))
model.add(layers.LSTM(24))
model.add(layers.Dense(1, activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam')
history = model.fit(x=x, y=y, epochs=10, verbose=1)