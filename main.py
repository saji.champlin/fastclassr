import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from pathlib import Path
import astroclass.data as data
import astroclass.model as model
import datetime
# data_path = Path("data/")
# lightcurves = list(data_path.glob("plasticc_test_lightcurves*.csv"))
# md = data_path / "plasticc_test_metadata.csv"
# data.convert_plasticc_dataset(data_path, md, lightcurves, use_gzip=False)
log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

datasetpath = Path("data/plasticc_test_lightcurves_05.parquet")
ds = data.get_tf_dataset(datasetpath, batch_size=50)

#get class weights
weight_arr = np.load('class_weights.npy')
# create a dict
weights_dict = {k: v for k, v in enumerate(weight_arr)} 

val_path = Path("data/plasticc_test_lightcurves_06.parquet")
val_data = data.get_tf_dataset(val_path,batch_size=50)
m = model.get_model(batch_size=50)
m.fit(ds, epochs=50, validation_data=val_data.take(100), class_weight=weights_dict)
m.save(log_dir + '/model')

in_batch = None
for i in ds:
    in_batch = i
    break
prediction = m(in_batch)

print(in_batch[0][0].shape)

print(np.argmax(in_batch[1],axis=1))

print(np.argmax(np.array(prediction), axis=1))