# checks dataset encoding

import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from pathlib import Path
import astroclass.data as data
import sklearn as sk

# Create a path to the test dataset.

datapath = Path("data/plasticc_train_lightcurves.parquet")
ds = data.get_tf_dataset(datapath)

for el in ds.take(2).as_numpy_iterator():
    print(el)

ds = data.get_tf_dataset(datapath, use_onehot=False)

for el in ds.take(2).as_numpy_iterator():
    print(el)

# # create a test one hot encoder
# test_y_classes = [1,3,4,7,9]
# test_lb = sk.preprocessing.LabelBinarizer()
# test_lb.fit(test_y_classes)

# test_dataset = tf.data.Dataset.from_tensor_slices([1,1,1,3,3,4,9,7])
# print("dataset pre mapping:" ,list(test_dataset.as_numpy_iterator()))

# test_dataset.map(lambda x: test_lb.transform(x))
# print("dataset post mapping " + list(test_dataset.as_numpy_iterator()))